package jp.alhinc.nabetani_manaka;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		Map<String, String> branchData = new HashMap<String, String>();
		Map<String,Long> salesData = new HashMap<String, Long>();
		
		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");
			if(!(file.exists())) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			br = new BufferedReader(new FileReader(file));
			String line;
			while((line = br.readLine()) != null) {
				String[] names = line.split(",");
				if(names.length == 2) {
					branchData.put(names[0], names[1]);
				} else {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}
		}
		catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			System.out.println(e);
		}
		finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
		
		BufferedReader br2 = null;
		try {
			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File file, String str) {
					if(str.matches("^[0-9]{8}.rcd$")) {
						return true;
					} else {
						return false;
					}
				}
			};
			
			File[] files = new File(args[0]).listFiles(filter);
			for(int i = 0; i < files.length; i++) {
				br2 = new BufferedReader(new FileReader(files[i]));
				String sale;
				
				List<String> salesDataList = new ArrayList<>();
				while((sale = br2.readLine()) != null) {
					salesDataList.add(sale);
				}
				
				//フォーマットチェック
				if(salesDataList.size() >= 3) {
					System.out.println("<" + files[i] + ">のフォーマットが不正です");
					return;
				}
				
				//支店コードチェック
				String code = salesDataList.get(0);
				if(!(branchData.containsKey(code))) {
					System.out.println("<" + files[i] + ">の支店コードが不正です" );
					return;
				}
				
				//合算
				Long data = Long.parseLong(salesDataList.get(1));
				Long sum = salesData.get(code);
				if(sum == null) {
					salesData.put(code, data);
				} else {
					sum += data;
					
					//桁数チェック
					if(String.valueOf(sum).length() > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					salesData.put(code, sum);
				}
			}
		}
		catch(IOException ioe) {
			System.out.println("予期せぬエラーが発生しました");
			System.out.println(ioe);
		}
		finally {
			if(br2 != null) {
				try {
					br2.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
		
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(args[0], "branch.out.txt")));
			for(Map.Entry<String, String> branchOut : branchData.entrySet()) {
			    for(Map.Entry<String, Long> salesOut : salesData.entrySet()) {
			    	if(branchOut.getKey().equals(salesOut.getKey())) {
			    		bw.write(branchOut.getKey() + "," + branchOut.getValue() + "," + salesOut.getValue() + "\r\n");
			    	}
			    }
		    }
			bw.close();
		}
		catch(IOException e) {
			System.out.println(e);
		}
		
	}
}
